json.extract! input, :id, :name, :deadline, :created_at, :updated_at
json.url input_url(input, format: :json)